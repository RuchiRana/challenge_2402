import java.math.BigInteger;
import java.util.Scanner;

public class Challenge_2402 {
	public static void main(String[] args) {
		System.out.println("Enter the Number:");
		BigInteger n = new BigInteger(new Scanner(System.in).nextLine());
		BigInteger two = new BigInteger("2");
		BigInteger j, i;
		int counter = 0;
		if (n.toString().length() <= 20 && n.toString().length()>0 ) {
			for (i = n.divide(two); ((i.compareTo(n.divide(two)) == 1 || i
					.compareTo(n.divide(two)) == 0)); i = i
					.subtract(BigInteger.ONE)) {
				for (j = n.divide(two); (j.compareTo(n) == -1 || j.compareTo(n) == 0); j = j
						.add(BigInteger.ONE)) {
					if (i.add(j).equals(n)
							&& String.valueOf(i).matches("^([012356789]+)$")
							&& String.valueOf(j).matches("^([012356789]+)$")
							&& i.toString().length() == j.toString().length()) {
						counter++;
						System.out.println("a=" + i + " ,b=" + j);
						System.out.println("end....");
						if (counter == 1)
							System.exit(0);
					}
				}
			}
		} else
			System.out.println("Please enter number of length less than or equal to 20");
	}
}
